define(['jquery', 'config', 'utils', 'conversation'],
function ($, Config, Utils, Conversation, undef) {
    "use strict";

    var Leward = window.Leward = {};

    Leward.config = Config;

    Leward.loadConfig = function (config) {
        Utils.request({
            resource: 'token',
            data: config,
            success: function (resp) {
                if (resp.ok) {
                    $.extend(Config, config);
                    Config.user = resp.data.user,
                    Config.token = resp.data.token;
                    Leward.conversation = function (opts) {
                        return new Conversation(opts);
                    };
                    if (typeof window.lewardReady == 'function') {
                        window.lewardReady();
                    }
                } else {
                    Utils.log.error(resp.errors);
                }
            }
        });
    }

    if (window.lewardConfig) {
        Leward.loadConfig(window.lewardConfig);
    }

    return Leward;
});