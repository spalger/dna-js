define(['jquery', 'notifiers/core', 'utils'],
function ($, Notifier, Utils, undef) {

    Notifier.prototype.initTransport = function (notify) {
        Utils.log.error("Polling transport has not been written yet.");
    }

    return Notifier;

});