define(['jquery', 'notifiers/core', 'socket.io', 'config'],
function ($, Notifier, SocketIO, Config, undef) {

    var io = SocketIO.connect( Config.notificationServer, {
                'resource':                  '',
                'transports':                [ 'websocket' ],
                'connect timeout':           1000,
                'try multiple transports':   false,
                'max reconnection attempts': 3,
             });


    Notifier.prototype.initTransport = function () {
        var notify = this;

        io.on('connect', $.proxy(notify, 'activate'));
        io.on('disconnect', $.proxy(notify, 'deactivate'));

        // listen for all stat packets for this conversation (identified by topic_id)
        io.on(notify.STATS_PREFIX + notify.topic_id, $.proxy(notify, 'distributeStats'));

        window.onUnload = function () {
            io.emit('doneListening', notify.topic_id);
        }

        notify.im = function (action) {
            io.emit(notify.ACTIONS[action], notify.topic_id);
        }

        if(io.socket.connected) {
            notify.activate();
        }

    }

    return Notifier;

});