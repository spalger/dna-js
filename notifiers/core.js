define(['jquery', 'config', 'utils', 'require'],
function ($, Config, Utils, require, undef) {
    'use strict';

    function Notifier (conv) {
        this.topic_id = conv.topic_id;
        this.typing = null;
        this.$ = $(this);

        this.$listening = conv.$composer.children('.listening');
        this.$typing = conv.$composer.find('.wrap .typing');

        this.$.on('typing', $.proxy(this, 'updateTyping'));
        this.$.on('listening', $.proxy(this, 'updateListening'));

        this.$textarea = conv.$composer.$textarea;
        this.initTransport();
    }

    Notifier.prototype = {
        STATS_PREFIX: 'S:',
        STATS: {
            L: 'listening',
            T: 'typing'
        },
        ACTIONS: {
            listening: 'L',
            doneListening: 'DL',
            typing: 'T',
            doneTyping: 'DT'
        },
        distributeStats: function (stats) {
            for (var type in stats) {
                if (stats.hasOwnProperty(type)) {
                    this.$.triggerHandler(this.STATS[type], stats[type]);
                }
            }
        },
        attachListener: function (ev, fn) {
            if ((ev === 'ready' && this.io.socket.connected) || (ev === 'notready' && !this.io.socket.connected)) {
                try { fn(); } catch (e) { Utils.log.error(e.message || 'ERROR: ', e); }
            } else {
                this.$.on(ev, fn);
            }
        },
        keepTrackOfMyTypingPlease: function () {
            var notify = this,
                doneTimeout;

            if (notify.typing !== null) {
                Utils.log.error('Already tracking typing...');
                return;
            } else {
                notify.typing = false;
            }

            function doneTyping() {
                clearTimeout(doneTimeout);
                if (notify.typing === true) {
                    notify.typing = false;
                    notify.im('doneTyping');
                }
            }

            function onTextAreaBlur() {
                clearTimeout(doneTimeout);
                if (notify.$textarea.text().length === 0) {
                    doneTyping();
                } else {
                    doneTimeout = setTimeout(doneTyping, 3000);
                }
            }

            function onTextAreaKeydown() {
                clearTimeout(doneTimeout);
                if (notify.typing === false) {
                    notify.typing = true;
                    notify.im('typing');
                }
                doneTimeout = setTimeout(doneTyping, 6000);
            }

            notify.stopTrackingTypingNOW = function () {
                clearTimeout(doneTimeout);
                notify.typing = null;
                notify.$textarea.off('blur', onTextAreaBlur).off('keydown', onTextAreaKeydown);
                notify.stopTrackingTypingNOW = $.noop;
            }

            notify.$textarea.on('blur', onTextAreaBlur).on('keydown', onTextAreaKeydown);
        },
        updateTyping: function (e, count) {
            var text = '', inactive = false;
            if (this.typing) {
                count--;
            }
            if (count === 1) {
                text = 'Someone else is ' + (this.typing ? 'also' : '' ) + ' typing a comment.';
            } else if (count > 1) {
                text = count + ' other people are ' + (this.typing ? 'also' : '' ) + 'typing comments.';
            } else {
                inactive = true;
            }
            this.$typing.toggleClass('inactive', inactive).text(text);
        },
        updateListening: function (e, count) {
            var text = '&nbsp;', inactive = false;
            count--;
            if (count > 0) {
                text = count + ' other ' + (count > 1 ? 'people are' : 'person is') + ' also viewing this.';
            } else {
                inactive = true;
            }
            this.$listening.toggleClass('inactive', inactive).html(text);
        },
        activate: function () {
            if (this.active === true) {
                Util.log.error('Already Active')
            }
            this.im('listening', this.topic_id);
            this.active = true;
            this.keepTrackOfMyTypingPlease();
            this.$listening.removeClass('inactive');
            this.$typing.removeClass('inactive');
        },
        deactivate: function () {
            this.active = false;
            this.stopTrackingTypingNOW();
            this.$listening.addClass('inactive');
            this.$typing.addClass('inactive');
        }
    }

    return Notifier;
});