define([
    'jquery',
    'utils',
    'config',
    'stylist',
    ('WebSocket' in window && window.WebSocket.CLOSING === 2 ? 'notifiers/websocket' : 'notifiers/polling'),
    'templates'
],
function ($, Utils, Config, Stylist, Notifier, Templates, undef) {
    'use strict';

    // opts
    //  - container
    //  - topic_id
    //  - user
    //  - nesting_limit (default 0)
    function Conversation(opts) {
        var conv = this, $conv = $(this);
        // conv.opts = opts;

        conv.user = opts.user
        conv.topic_id = opts.topic_id;
        conv.nesting_limit = opts.nesting_limit || 0;
        conv.$container = $(opts.container);
        conv.comments = {};
        conv.users = {};
        conv.users[Config.user.id] = Config.user;
        conv.$comments = conv.$container.find('ul.comments');
        conv.$commentTemplate = conv.$container.find('.comment').remove().show();

        conv.$composer = conv.$container.find('.composer');
        conv.$composer.stylist = new Stylist(conv.$composer);

        conv.$replyComposer = conv.$composer.clone().addClass('reply');
        conv.$replyComposer.children('.listening')
            .remove().end()
            .find('.wrap .typing')
            .remove();
        conv.$replyComposer.find('button.post')
            .removeClass('post')
            .addClass('postReply')
            .html('Post Reply')
            .after('<button class="cancelReply">Cancel</button>');
        conv.$replyComposer.stylist = new Stylist(conv.$replyComposer);

        conv.$container.on('click', '.popup-menu > button', function(e){
            var $this = $(this), $par = $this.parent();
            if ($par.toggleClass('open').is('.open')) {
                e.stopImmediatePropagation();
                conv.$container.find('.popup-menu.open').not($par).removeClass('open');
                $(document).one('click', function(){
                    $this.parent().removeClass('open');
                });
            }
        });

        conv.$composer.on('click', 'button.post', $.proxy(conv, "submitComment"));

        conv.$comments.on('click', 'button.postReply', $.proxy(conv, "replyToComment"));
        conv.$comments.on('click', 'button.cancelReply', $.proxy(conv, "cancelReply"));
        conv.$comments.on('click', '.actions button.reply', $.proxy(conv, "showReplyComposer"));
        conv.$comments.on('click', '.actions button.share', $.proxy(conv, "shareComment"));
        conv.$comments.on('click', '.actions button.report', $.proxy(conv, "reportComment"));

        if (conv.nesting_limit) {
            conv.$comments.on('click', '.show_replies button', conv.showTheseReplies);
        }

        if (Config.user.is_moderator) {
            conv.$comments.on('click', '.admin-actions .remove-comment', $.proxy(conv, "removeComment"));
        }

        conv.notify = new Notifier(conv);

        Utils.request({
            resource: 'conversation',
            data: {
                topic_id: conv.topic_id
            },
            success: function (resp) {
                var html = '', i, users, comments;
                if (resp.ok) {

                    users = (resp.data && resp.data.users) || [];
                    for (i = 0; i < users.length; i++) {
                        conv.users[users[i].id] = users[i];
                    }

                    comments = (resp.data && resp.data.comments) || [];
                    for (i = 0; i < comments.length; i++) {
                        html += conv.makeCommentHTML(comments[i]);
                    }
                    conv.$comments.html(html);
                }
            }
        });
    }

    Conversation.prototype = {
        anonUser: {
            id: 'anon',
            user_id: 'anon',
            display_name: 'anon',
            image_url: '',
            profile_url: ''
        },
        makeCommentHTML: function (comment, level) {
            var i, childHTML='', hiddenChildrenCount = false,
                user = this.users[comment.user] || this.anonUser;
            this.comments[comment.id] = comment;
            if (level === undef) {
                level = 1;
            }
            if (comment.children.length) {
                if (this.nesting_limit && this.nesting_limit <= level) {
                    hiddenChildrenCount = comment.children.length;
                }
                for (i = 0; i < comment.children.length; i++) {
                    childHTML += this.makeCommentHTML(comment.children[i], level+1);
                }
            }
            return Templates.comment(comment, user, childHTML, hiddenChildrenCount);
        },
        highlightComment: function($comment){
            $comment.addClass('highlighted');
            setTimeout(function(){
                $comment.removeClass('highlighted');
            }, 5000);
        },
        scrollToComment: function($comment, $parent) {
            var parentY = $parent ? $parent.offset().top : 0,
                commentY = $comment.offset().top,
                winTop = $(window).scrollTop(),
                winHeight = $(window).height(),
                scrollTop;

            if (commentY > winTop + Math.round(winHeight * .8)) {
                $(document.body).animate({
                    scrollTop: commentY - Math.round(winHeight * .45)
                }, {
                    duration: 200,
                    complete: $.proxy(this, "highlightComment", $comment)
                });
            } else {
                this.highlightComment($comment);
            }
        },
        submitComment: function (event) {
            if (Utils.trim(this.$composer.$textarea.text()).length === 0) {
                return false;
            }
            var comment = {
                    content: Utils.trim(this.$composer.$textarea.html()),
                    created_at: Utils.unixtime(),
                    children: [],
                    user: Config.user.id
                },
                $comment = $(this.makeCommentHTML(comment));

            this.$composer.$textarea.html('');
            $comment.addClass('pending').find('.actions button').attr('disabled', 'disabled');
            this.$comments.append($comment);
            this.scrollToComment($comment);
            Utils.request({
                resource: 'comment',
                method: 'post',
                data: {
                    content: comment.content,
                    topic_id: this.topic_id
                },
                success: function (resp) {
                    if (resp.ok) {
                        $comment.removeClass('pending').find('.actions button').removeAttr('disabled').end();
                    } else {
                        Utils.log.error(resp.errors);
                    }
                }
            });
        },
        showReplyComposer: function (event) {
            var $parent = $(event.target).closest('.comment'), end,
                parent_user = this.users[$parent.data('user')],
                parent_names = parent_user.display_name.split(' '),
                parent_user_id = parent_user.id, i;

            for(i = 0; i < parent_names.length; i++) {
                parent_names[i] = '<a data-user-id="'+parent_user_id+'" contenteditable="false">' + (i === 0 ? '@' : ' ') + parent_names[i] + '</a>';
            }
            this.$replyComposer.$textarea.html(parent_names.join('') + '&nbsp;');
            $parent.children('.replies').prepend(this.$replyComposer);
            this.scrollToComment(this.$replyComposer);

            if (window.getSelection && document.createRange) {
                end = this.$replyComposer.$textarea.get(0).lastChild;
                var sel = window.getSelection();
                var range = document.createRange();
                range.setEndAfter(end);
                range.collapse(false);
                sel.removeAllRanges();
                sel.addRange(range);
            } else if (document.selection && document.body.createTextRange) {
                end = this.$replyComposer.$textarea.children().get(-1);
                var textRange = document.body.createTextRange();
                textRange.moveToElementText(end);
                textRange.moveEnd('character', 1);
                textRange.collapse(false);
                textRange.select();
            }

        },
        cancelReply: function (event) {
            this.$replyComposer.detach();
        },
        replyToComment: function (event) {
            if (Utils.trim(this.$replyComposer.$textarea.text()).length === 0) {
                return false;
            }
            var comment = {
                    content: this.$replyComposer.$textarea.html(),
                    created_at: Utils.unixtime(),
                    children: [],
                    user: Config.user.id
                },
                $parent = $(event.target).closest('.comment'),
                $comment = $(this.makeCommentHTML(comment)),
                commentY, parentY;


            $comment.addClass('pending').find('.actions button').attr('disabled', 'disabled').end();

            this.$replyComposer.detach();

            $parent.children('.replies').append($comment);
            this.scrollToComment($comment, $parent);
            Utils.request({
                resource: 'comment',
                method: 'post',
                data: {
                    content: comment.content,
                    topic_id: this.topic_id,
                    parent_id: $parent.data('comment')
                },
                success: function (resp) {
                    if (resp.ok) {
                        $comment.removeClass('pending').find('.actions button').removeAttr('disabled');
                        $comment = null;
                    } else {
                        Utils.log.error(resp.errors);
                    }
                }
            });
        },
        removeComment: function (ev) {
            var $comment = $(ev.target).closest('.comment');
            Utils.request({
                resource: 'comment',
                method: 'delete',
                data: {
                    id: $comment.data('comment')
                },
                success: function () {
                    $comment.html('This comment has been removed');
                }
            });
        },
        shareComment: function () {
            Utils.log.error('not implemented');
        },
        reportComment: function () {
            Utils.log.error('not implemented');
        },
        showTheseReplies: function () {
            $(this).parent().next().show().end().hide();
        }
    };

    return Conversation;
})