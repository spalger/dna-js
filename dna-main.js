require.config({
    paths: {
        jquery: (window.jQuery && jQuery().jquery.match(/^1\.[7-9]/) ? 'local-jquery' : 'http://dna.hawkburn.com/jquery'),
        'socket.io': 'http://dna.hawkburn.com/socket.io'
    }
})

// must call leward, that kickstarts the whole deal
define(['jquery', 'utils', 'leward'], function ($, Utils) {
    Utils.log = $.noop;
    Utils.log.error = function () {
        if ($.isFunction(console.error)) {
            console.error.apply(console, $.makeArray(arguments));
        }
    }
});