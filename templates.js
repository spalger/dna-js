define(['utils', 'config'],
function (Utils, Config) {
    var templates = {
        comment: function (comment, user, childHTML, hiddenChildrenCount) {
            return  '<li class="comment" data-comment="'+comment.id+'" data-user="'+user.id+'">' +
                        '<div class="wrap">' +
                            '<p class="content">'+comment.content+'</p>' +
                            '<p class="tagline">' +
                                (user.image_url ? '<img class="user_image" src="'+user.image_url+'" >' : '') +
                                'From <a class="user_link">'+user.display_name+'</a> at <span class="time">'+Utils.unixtime(comment.created_at)+'</span>' +
                            '</p>' +
                            '<div class="actions">' +
                                '<button class="reply" type="button" title="Reply"><i class="icon-reply"></i></button>' +
                                '<button class="share" type="button" title="Share"><i class="icon-share"></i></button>' +
                                '<button class="report" type="button" title="Report"><i class="icon-minus-sign"></i></button>' +
                                '<span class="divider"></span>' +
                                ( Config.user.is_moderator ? templates.moderatorMenu() : '' ) +
                            '</div>' +
                        '</div>' +
                        (hiddenChildrenCount ? templates.showReplies(hiddenChildrenCount) : '') +
                        '<ul class="replies"'+(hiddenChildrenCount ? ' style="display:none;"' : '')+'>'+childHTML+'</ul>' +
                    '</li>';
        },
        showReplies: function (count) {
            return '<div class="show_replies"><button><i class="icon-plus"></i> Show '+count+' replies</button></div>';
        },
        moderatorMenu: function () {
            return  '<div class="popup-menu">' +
                        '<button>Admin Actions<i class="icon-caret-down"></i></button>' +
                        '<ul class="menu admin-actions">' +
                            '<li><i class="icon-user"></i> View User info</li>' +
                            '<li><i class="icon-legal"></i> Ban this dude</li>' +
                            '<li><i class="icon-exclamation-sign"></i> Mark as a menice</li>' +
                            '<li class="remove-comment"><i class="icon-trash"></i> Trash it</li>' +
                        '</ul>' +
                    '</div>';
        }
    };
    return templates;
});