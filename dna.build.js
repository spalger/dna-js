({
    // appDir: '../',
    baseUrl: './',
    // dir: '../../frontend-build',
    //Comment out the optimize line if you want
    //the code minified by UglifyJS
    // optimize: 'none',
    paths: {
        'socket.io': 'empty:',
        'requireLib': '../require',
        'jquery': 'empty:',
        'notifier': 'empty:'
    },
    include: [
        'requireLib',
        'notifiers/core',
        'notifiers/polling',
        'notifiers/websocket'
    ],
    name: 'dna-main',
    out: '../dna.js'
})