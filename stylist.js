define(['jquery'],
function ($, undef) {
    'use strict';

    function Stylist($composer) {
        this.$composer = $composer;
        // get references to the elements that are needed often
        $composer.$textarea = $composer.find('.input');
        $composer.$bold = $composer.find('button.bold');
        $composer.$italic = $composer.find('button.italic');
        $composer.$underline = $composer.find('button.underline');
        $composer.$strikethrough = $composer.find('button.strikethrough');

        // set event handlers for the styler buttons
        $composer.$bold.click(this.styleButtonEventHandler("Bold"));
        $composer.$italic.click(this.styleButtonEventHandler("Italic"));
        $composer.$underline.click(this.styleButtonEventHandler("Underline"));
        $composer.$strikethrough.click(this.styleButtonEventHandler("StrikeThrough"));
        $composer.$textarea.on('mouseup keyup', $.proxy(this.updateComposerActions, this, $composer));
    }

    Stylist.prototype = {
        styleButtonEventHandler: function (style) {
            var stylist = this;
            return function(event){
                var target = (event.target.nodeName === 'I') ? event.target.parentElement : event.target;
                $(target).toggleClass('active');
                stylist.setStyle(style);
            }
        },
        setStyle: function (style) {
            if (this.$composer.$textarea.get(0) !== document.activeElement) {
                this.$composer.$textarea.get(0).focus();
            }
            document.execCommand(style, false, null);
        },
        updateComposerActions: function ($composer) {
            var focus, $parents, i, styles;
            if (document.queryCommandState) {
                $composer.$bold.toggleClass('active', document.queryCommandState('Bold'));
                $composer.$italic.toggleClass('active', document.queryCommandState('Italic'));
                $composer.$underline.toggleClass('active', document.queryCommandState('Underline'));
                $composer.$strikethrough.toggleClass('active', document.queryCommandState('StrikeThrough'));
            } else if (window.getSelection) {
                focus = window.getSelection().focusNode;
                if (focus) {
                    styles = {};
                    while(focus !== null && focus.nodeName !== 'DIV') {
                        if (focus.nodeType === 1) {
                            focus.styles = window.getComputedStyle(focus);
                            if (!styles.hasOwnProperty('bold')) {
                                styles.bold = focus.styles.fontWeight === 'bold';
                                styles.italic = focus.styles.fontStyle === 'italic';
                            }
                            if (styles.underline === undef) {
                                if (focus.styles.textDecoration === 'underline') {
                                    styles.underline = true;
                                }
                            }
                            if (styles.underline === undef) {
                                if (focus.styles.textDecoration === 'line-through') {
                                    styles.strike = true;
                                }
                            }
                        }
                        if (styles.underline !== undef && styles.strike !== undef) break;
                        focus = focus.parentElement;
                    }
                    $composer.$bold.toggleClass('active', !!styles.bold);
                    $composer.$italic.toggleClass('active', !!styles.italic);
                    $composer.$underline.toggleClass('active', !!styles.underline);
                    $composer.$strikethrough.toggleClass('active', !!styles.strike);
                }
            } else if (document.selection) { // IE
                focus = document.selection.createRange();
                $composer.$bold.toggleClass('active', focus.queryCommandState('Bold'));
                $composer.$italic.toggleClass('active', focus.queryCommandState('Italic'));
                $composer.$underline.toggleClass('active', focus.queryCommandState('Underline'));
                $composer.$strikethrough.toggleClass('active', focus.queryCommandState('StrikeThrough'));
            }
        }
    }

    return Stylist;
});