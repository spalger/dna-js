define(['jquery', 'config'],
function ($, Config, undef) {

    var Utils = {};

    Utils.log = function () {
        if ($.isFunction(console.log)) {
            console.log.apply(console, $.makeArray(arguments));
        }
    };

    Utils.log.error = function () {
        if ($.isFunction(console.error)) {
            console.error.apply(console, $.makeArray(arguments));
        }
        else if ($.isFunction(console.log)) {
            var args = $.makeArray(arguments);
            args.unshift('ERROR:: ');
            console.log.apply(console, args);
        }
    }

    Utils.request = function (opts) {
        if (Config.token && !opts.data.token) {
            opts.data.token = Config.token;
        }
        if (opts.method) {
            opts.method = opts.method.toLowerCase();
            if (opts.method !== 'post' && opts.method !== 'get') {
                opts.data.method = opts.method;
                opts.method = 'get';
            }
        }
        return $.ajax({
            url: Config.apiDomain + opts.resource,
            type: opts.method || 'get',
            dataType: 'json',
            data: opts.data,
            success: opts.success,
            error: opts.error || Utils.log.error
        });
    }

    Utils.now = (Date.now || function(){ return (new Date).getTime(); });

    Utils.unixtime = function (ts) {
        if (ts === undef) {
            return Math.floor(Utils.now()/1000);
        } else {
            return (new Date(ts * 1000)).toLocaleString();
        }
    }

    Utils.trim = function (str) {
        str = str.replace(/^\s+/, '');
        for (var i = str.length - 1; i >= 0; i--) {
            if (/\S/.test(str.charAt(i))) {
                str = str.substring(0, i + 1);
                break;
            }
        }
        return str;
    }

    return Utils;

});